<?php
/*
 Plugin Name: Custom Post
Plugin URI: http://
Description: A simple Custom Post
Version: 1.0
Author: Abdullah All Mehedi
Author URI: http://
License: GPLv2 or later
*/

/*
 This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/



register_activation_hook( __FILE__, array( 'Cp_Setup', 'cpActivatePlugin' ) );
register_deactivation_hook( __FILE__, array( 'Cp_Setup', 'cpDeactivatePlugin' ) );
register_uninstall_hook( __FILE__, array( 'Cp_Setup', 'cpUnisntallPlugin' ) );


add_action( 'plugins_loaded', array( 'Cp_Setup', 'init' ) );
class Cp_Setup{
	
	public static function init(){
		
	}
	
	public static function cpActivatePlugin(){
		
	}
	
	public static function cpDeactivatePlugin(){
		
	}
	
	public static function cpUninstallPlugin(){
		
	}
}